﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asangard.Models;
using System.Data.Entity;
using System.Net;

namespace asangard.Controllers
{
    public class ProfileController : Controller
    {
        TravelEntities tr = new TravelEntities();


      [Authorize(Roles = "Admin")]
        public ActionResult AdminPage()
        {

            return View();
        }

        [Authorize(Roles = "User")]
        public ActionResult UserPage()
        {
            string user = User.Identity.Name;

            

            var id = tr.Persons.Where(p => p.UserName == user).Select(m => m.IdPerson).Single();


            return View(tr.ListTours.Where(m => m.IdPerson == id).ToList());
            

        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public ActionResult EditProfile()
        {
            string user = User.Identity.Name;
            int id = tr.Persons.Where(p => p.UserName.Equals(user)).Select(o=>o.IdPerson).Single();

            //if (id == 0)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            Person person = tr.Persons.Find(id);
            //if (person == null)
            //{
            //    return HttpNotFound();
            //}
            return View(person);


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile([Bind(Include = "IdPerson,UserName,FullName,Email,Password")] Person person)
        {
            if (ModelState.IsValid)
            {
                tr.Entry(person).State = EntityState.Modified;
                tr.SaveChanges();
                return RedirectToAction("UserPage");
            }
            return View(person);


        }


        [Authorize(Roles = "User")]
        [HttpGet]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ListTour lt = tr.ListTours.Find(id);
            if (lt == null)
            {
                return HttpNotFound();
            }
            return View(lt);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            ListTour lt = tr.ListTours.Find(id);
            tr.ListTours.Remove(lt);
            tr.SaveChanges();
            return RedirectToAction("UserPage");
        }
       


        }
    }