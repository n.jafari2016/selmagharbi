﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using asangard.Models;
using System.IO;

namespace asangard.Controllers
{
    public class ToursController : Controller
    {
        private TravelEntities db = new TravelEntities();
        private toorEntities ds = new toorEntities();

        // GET: Tours
        public ActionResult Index()
        {
            return View(ds.Table1.ToList());
        }

        // GET: Tours/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tour tour = db.Tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        // GET: Tours/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tours/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdTour,Location,Cost,text,date")] Table1 tour)
        {

           
            if (ModelState.IsValid)
            {
                int id = 0;
            var count = ds.Table1.Count();
                
                if(count==0)
                {

                    id = 0;
                }
                else
                {
                    id = ds.Table1.Max(p => p.IdTour);

                }
               
                tour.IdTour = id + 1;
                //tour.date = DateTime.Now;

                ds.Table1.Add(tour);

                ds.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(tour);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult AddPicture(int? id)
        {
            TempData["id"] = id;
           var city = db.Tours.Where(p => p.IdTour.Equals(id)).Select(o => o.Location);
           var cost = db.Tours.Where(p => p.IdTour.Equals(id)).Select(o => o.Cost);
            TempData["loc"] = city;

             TempData["cost"] = cost;

            //var path = db.Pictures.Where(p => p.Location.Equals(city)).Count();

            //if(path == 0)
            //{

                return View();
            //}

            //else
            //{

            //    return View(db.Pictures.Where(i => i.Location.Equals(city)).ToList());
            //}

        

        }


        [HttpPost]
        public ActionResult AddPicture(HttpPostedFileBase file,string Location)
        {
            var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "jpeg" };
            picEntities Pe = new picEntities();
            Picture pic = new Picture();
           

            if (file != null)
            {
                // BookPic tbl = new BookPic();
                ////tbl.Image = file1.ToString(); //getting complete url 
                ////tbl.name = fc["Name"].ToString();

                var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg) 
                var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg) 
                if (allowedExtensions.Contains(ext)) //check what type of extension 
                {

                    string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension 
                    string myfile = name + "_" + TempData["id"] + ext; //appending the name with id 
                                                                       //    // store the file inside ~/project folder(Img) 
                                                                       //    var path = Path.Combine(Server.MapPath("~/Img"), myfile);
                                                                       //   // var path = Url.Content("~img");


                    int max = 0;

                    var path = "~/uploads/" + name;

                    //pic.Path = path;
                    //pic.Location = Location;
                    if (Pe.Pictures.Count() == 0)
                    {
                        pic.IdPic = 0;
                    }
                    else
                    {
                        max = Pe.Pictures.Max(k => k.IdPic);
                    }

                    pic.IdPic = max + 1;
                    pic.Location = Location;
                    pic.Path = path;

                    Pe.Pictures.Add(pic);
                    Pe.SaveChanges();
             
                   // int x = max + 1;

                    path = Path.Combine(Server.MapPath("~/upload/"), myfile);
                    file.SaveAs(path);

                  // Picture p = Pe.Pictures.Find(x);
             //  var x = Pe.Pictures.Where(i => i.Location.Equals(pic.Location)).ToList();
                //    return View("Index");

                   return View("Pic",Pe.Pictures.Where(i => i.Location.Equals(pic.Location)).ToList());
                }
                else
                {
                    ViewBag.message = "file is invalid";
                }

            }

            return RedirectToAction("Index");

        }

       

        // GET: Tours/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tour tour = db.Tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        // POST: Tours/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdTour,Location,Cost,text")] Tour tour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tour);
        }

        // GET: Tours/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tour tour = db.Tours.Find(id);
            if (tour == null)
            {
                return HttpNotFound();
            }
            return View(tour);
        }

        // POST: Tours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tour tour = db.Tours.Find(id);
            db.Tours.Remove(tour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
