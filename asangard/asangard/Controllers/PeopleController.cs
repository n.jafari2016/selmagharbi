﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using asangard.Models;

namespace asangard.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PeopleController : Controller
    {
        private TravelEntities db = new TravelEntities();

        // GET: People
     
        public ActionResult Index()
        {
            return View(db.Persons.ToList());
        }

        // GET: People/Details/5
     
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPerson,UserName,FullName,Email,Password")] Person person)
        {
            if (ModelState.IsValid)
            {
                var count = db.Persons.Count();
                int id = 0;
                if (count==0)
                {
                    person.IdPerson = id;

                }
                else
                {
                    id = db.Persons.Max(p => p.IdPerson);
                    person.IdPerson = id + 1;
                }
               

                db.Persons.Add(person);
                db.SaveChanges();

                User user = new User();
                UserRole ur = new UserRole();
                user.UserID = person.IdPerson;
                user.Username = person.UserName;
                user.Password = person.Password;
                user.FullName = person.FullName;
                user.Email = person.Email;

                ur.UserID = user.UserID;
                ur.RoleID = 2;
                ur.UserRolesID = user.UserID;

                db.Users.Add(user);
                db.UserRoles.Add(ur);
                db.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPerson,UserName,FullName,Email,Password")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.Persons.Find(id);
            db.Persons.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

     

    }
}
