﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using asangard.Models;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Security;
using asangard.ViewModel;
using System.Net;

namespace asangard.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        TravelEntities tr = new TravelEntities();
        picEntities pe = new picEntities();
        // GET: Home
        public ActionResult Index()//صفحه اصلی
        {
            ViewBag.browser = Request.Browser.Browser;
            ViewBag.ip = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
            ViewBag.verzhen = Request.UserAgent;
            return View();
        }

        [HttpGet]
        public ActionResult Index2()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index2(string Location)//تورها
        {
            //   return PartialView(dbce.meetings.OrderByDescending(e => e.DateMeeting).Take(5));

            var count = tr.Tours.Where(p => p.Location.Equals(Location)).Count();

            //for (int i = 0; i < count; i++)
            //{
            //    tr.TourPics.Where(p =>p.IdPic.Equals())

            //}

            if (count != 0)
            {


                var tid = tr.Tours.Where(l => l.Location.Equals(Location)).ToList();
    

                return View("List", tid);
            }


            else
            {
                
                return View("List2");
            }


            //  return View("List",tr.Tours.Where(p => p.Location.Equals(Location)).ToList());
        }



        public ActionResult Call()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {


            return View();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "IdPerson,UserName,FullName,Email,Password")] Person person)
        {
            if (ModelState.IsValid)
            {
                if (! tr.Users.Any(u => u.Username == person.UserName.Trim().ToLower()))
                {

                    int id = tr.Persons.Max(p => p.IdPerson);
                    person.IdPerson = id + 1;

                    tr.Persons.Add(person);
                     tr.SaveChanges();

                    User user = new User();
                    UserRole ur = new UserRole();
                    user.UserID = person.IdPerson;
                    user.Username = person.UserName;
                    user.Password = person.Password;
                    user.FullName = person.FullName;
                    user.Email = person.Email;

                    ur.UserID = user.UserID;
                    ur.RoleID = 2;
                    ur.UserRolesID = user.UserID;

                    tr.Users.Add(user);
                    tr.UserRoles.Add(ur);
                    tr.SaveChanges();

                    TempData["log"] = "welcome to asangard,please sign in for see your profile";


                }

                else
                {
                    TempData["repeated"] = "invalid username ";

                    return View();

                }
            }

            return RedirectToAction("sign");
         
        }

        /// <summary>
        /// ////logiiiiiiiiiiiiiiiiiiiin///////////////////////////////////
        /// </summary>

        /// <returns></returns>
        /// 

        [HttpGet]
        public ActionResult sign(string ReturnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (shouldRedirect(ReturnUrl))

                {
                    return Redirect(ReturnUrl);

                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult sign(signin l, string ReturnUrl)
        {


            if (ModelState.IsValid)
            {


                User person = tr.Users.FirstOrDefault(u => u.Username.Equals(l.Username) && u.Password.Equals(l.Password));

                if (person != null)

                {
                    FormsAuthentication.SetAuthCookie(l.Username, l.RememberMe);




                    UserRole myrole = tr.UserRoles.Find(person.UserID);

                    if (myrole.RoleID == 1)

                    {


                        return RedirectToAction("AdminPage", "Profile");
                    }

                    else
                    {
                        return RedirectToAction("UserPage", "Profile");
                    }


                }
                else
                {
                    this.ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    ViewBag.Error = "Login faild! Make sure you have entered the right user name and password!";
                    return View();

                }
            }


            ModelState.Remove("Password");
            return RedirectToAction("Errorlog");

        }

        public ActionResult Errorlog()
        {
            ViewBag.Errorlog = "try again";
            return View("sign");
        }
        private bool shouldRedirect(string returnUrl)//آدرس بازگشت
        {
            return !string.IsNullOrWhiteSpace(returnUrl) &&
        Url.IsLocalUrl(returnUrl) &&
            returnUrl.Length > 1 &&
            returnUrl.StartsWith("/") &&
            !returnUrl.StartsWith("//") &&
            !returnUrl.StartsWith("/\\");
        }
        [Authorize]
        public ActionResult signout()
        {
            FormsAuthentication.SignOut();
            TempData["signout"] = "please sign in system :)";


            return RedirectToAction("sign");

        }

        

        [HttpGet]
        public ActionResult buy(int id)//id tour
        {

            Tour to = new Tour();

            to.Location = tr.Tours.Where(p => p.IdTour.Equals(id)).Select(i => i.Location).Single();
            to.Cost = tr.Tours.Where(p => p.IdTour.Equals(id)).Select(i => i.Cost).Single();

            Person per = new Person();
            ListTour lt = new ListTour();


            string name = User.Identity.Name;
            per.IdPerson = tr.Persons.Where(p => p.UserName == name).Select(m => m.IdPerson).Single();

            lt.IdList = id;
            lt.IdPerson = per.IdPerson;
            lt.IdTour = id;
            lt.Date = (Extention.ToPersian(DateTime.Now)).ToString();
            lt.Cost = to.Cost; ;
            lt.Location = to.Location;

            tr.ListTours.Add(lt);
            tr.SaveChanges();



            return RedirectToAction("UserPage", "Profile");


        }


      
    }
}