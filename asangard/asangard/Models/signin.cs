﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace asangard.Models
{
    public class signin
    {


        [Required(ErrorMessage = "نام کاربری را وارد کنید.", AllowEmptyStrings = false)]
        [Key]
        [Display(Name = "نام کاربری ")]
        public string Username
        {
            get;
            set;
        }

        [Display(Name = "رمزعبور ")]
        [Required(ErrorMessage = "رمزعبور را وارد کنید.", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "رمزعبومرا به یاد داشته باش ")]
        public bool RememberMe
        {
            get;
            set;
        }

    }
}