﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using asangard.Models;

namespace asangard.ViewModel
{
    public class PicInfo
    {
        public IEnumerable<Tour> tours { get; set; }
        public IEnumerable<Picture> pictures { get; set; }

    }
}